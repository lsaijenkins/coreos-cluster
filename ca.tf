# puppet Certificate Authority

resource "tls_private_key" "puppet_ca_key" {
  algorithm = "ECDSA"
}

resource "tls_self_signed_cert" "puppet_ca_cert" {
  key_algorithm     = "ECDSA"
  private_key_pem   = "${tls_private_key.puppet_ca_key.private_key_pem}"
  is_ca_certificate = true

  subject {
    common_name  = "puppet CA"
    organization = "LSAI"
  }

  validity_period_hours = 10000

  allowed_uses = [
    "any_extended",
    "cert_signing",
    "key_encipherment",
    "digital_signature",
    "server_auth",
  ]
}
