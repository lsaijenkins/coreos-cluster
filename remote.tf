resource "null_resource" "cluster" {
  triggers {
    trigger = "${base64encode(format("%s:%s", tls_private_key.puppet_ca_key.private_key_pem, var.etcd_servers))}"
  }

  count = "${length(split(",",var.etcd_servers))}"

  connection {
    host = "${element(split(",",var.etcd_servers), count.index)}"
    user = "core"
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir -p /var/lib/puppet",
    ]
  }
}
